package com.datediff.datediffcalculator;

public class Date {

    private int year;
    private int month;
    private int day;

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }
    
    public Date(String date) throws IllegalArgumentException {
        String[] items = date.split("-");
        if (items.length != 3) throw new IllegalArgumentException();
        int yy = Integer.parseInt(items[0]);
        int mm = Integer.parseInt(items[1]);
        int dd = Integer.parseInt(items[2]);
        if (!isValid(yy, mm, dd)) throw new IllegalArgumentException();
        this.year = yy;
        this.month = mm;
        this.day = dd;
    }
    
    public static boolean isValid(int year, int month, int day) {
        
        boolean isValid = true;
        
        if( (year < 1901) 
                || (year > 2999) 
                || (month < 1) 
                || (month > 12)
                || (day < 1) 
                || (day > 31)) 
        {
            isValid = false;
        }
        switch (month) {
            case 2: 
            isValid = (isLeapYear(year) ? day <= 29 : day <= 28);
            break;
            case 4: 
            isValid = day < 31;
            break;
            case 6: 
            isValid = day < 31;
            break;
            case 9: 
            isValid = day < 31;
            break;
            case 11: 
            isValid = day < 31;
            break;
        } 
        return isValid;
    }
    
    public static boolean isLeapYear(int year) {
        return ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0));
    }
    
    public long getJulianDay() {
       return ((1461 * (year + 4800 + (month - 14)/12))/4 +
               (367 * (month - 2 - 12 * ((month - 14)/12)))/12 - 
               (3*((year + 4900 + (month - 14)/12)/100))/4 + day - 32075);
    }
}
