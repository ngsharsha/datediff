### What is this repository for? ###

* Calculates days elapsed between two dates
* 1.0.0

### How do I get set up? ###

### Prerequisites
* JDK 8 installed and make sure JAVA_HOME environment variable is set
* Maven 3 or higher

### To build & execute test cases, execute the following commands
* mvn clean install

### To run the application, from the root folder execute the following command
* java -jar  target/DateDiffCalculator-1.0-SNAPSHOT.jar