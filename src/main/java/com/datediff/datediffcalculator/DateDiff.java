package com.datediff.datediffcalculator;

import java.util.Scanner;

public class DateDiff {
 
    public static void main (String[] args) {
        startApp();
    }

    
    private static void startApp() {
        
        System.out.println("Enter two dates b/w 1901-01-01 and 2999-12-31 "
                + "in YYYY-MM-DD format");
        
        try (Scanner sc = new Scanner(System.in)) {
            
            System.out.println("Enter first Date");
            String strDate1 = sc.next();
            
            /*This will also ensure validating the date range and format of the 
            date and throws an InvalidArgument exception in case of failure */
            Date date1 = new Date(strDate1);
            
            System.out.println("Enter second Date");
            String strDate2 = sc.next();
            Date date2 = new Date(strDate2);
        
            /* This method will calculate the difference 
            between two dates by evaluating Julian Day Number of each date 
            and the end we subtract one day because of the requirement not to 
            count the first and last day*/
            
            long result = DateUtils.calculateDiff(date1, date2) - 1;
            
            System.out.println("Elapsed Days: " + result);
            
        } catch (IllegalArgumentException ex) {
            System.out.println("The Date you have entered is not valid");
        }
    }
}