package com.datediff.datediffcalculator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class DateDiffTest {
    
    private List<String> dates = new ArrayList<>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public DateDiffTest() {
    }
    
    @Before
    public void setUp() throws Exception {
        dates = Arrays.asList("1983-06-02", "1983-06-22", "1984-07-04", 
                "1984-12-25", "1983-08-03", "1989-01-03");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testDiffDays() throws ParseException {
        
        System.out.println("testDiffDays");
        
        for(int i =0; i<dates.size()-1 ; i+=2) {
            
            String strDate1 = dates.get(i);
            String strDate2 = dates.get(i+1);
            System.out.println("Test Input Dates " + strDate1 + " " + strDate2);
            Date date1 = new Date (strDate1);
            Date date2 = new Date (strDate2);
            long diffDays = DateUtils.calculateDiff(date1, date2);

            java.util.Date utilDate1 = dateFormat.parse(strDate1);
            java.util.Date utilDate2 = dateFormat.parse(strDate2);
            long diffInMillies = Math.abs(utilDate1.getTime() - utilDate2.getTime());
            long diffUtilDays = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

            assertEquals(diffUtilDays, diffDays);
        }
    }
}
    