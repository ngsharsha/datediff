package com.datediff.datediffcalculator;

import org.junit.Test;
import static org.junit.Assert.*;

public class DateTest {
    
    public DateTest() {
    }
    
    @Test
    public void testIsValidLowerbound() {
        System.out.println("testIsValidLowerbound");
        int year = 1900;
        int month = 01;
        int day = 01;
        boolean result = Date.isValid(year, month, day);
        assertFalse(result);
    }
    
    @Test
    public void testIsValidUpperbound() {
        System.out.println("testIsValidUpperbound");
        int year = 3000;
        int month = 01;
        int day = 01;
        boolean result = Date.isValid(year, month, day);
        assertFalse(result);
    }
    
    @Test
    public void testIsValid31Days() {
        System.out.println("testIsValid31Days");
        int year = 1994;
        int month = 01;
        int day = 31;
        boolean result = Date.isValid(year, month, day);
        assertTrue(result);
    }
    
    @Test
    public void testIsValid30Days() {
        System.out.println("testIsValid30Days");
        int year = 1994;
        int month = 06;
        int day = 31;
        boolean result = Date.isValid(year, month, day);
        assertFalse(result);
    }
    
    @Test
    public void testIsValid29Days() {
        System.out.println("testIsValid29Days");
        int year = 1992;
        int month = 02;
        int day = 29;
        boolean result = Date.isValid(year, month, day);
        assertTrue(result);
    }
    
    @Test
    public void testIsValid28Days() {
        System.out.println("testIsValid28Days");
        int year = 1993;
        int month = 02;
        int day = 29;
        boolean result = Date.isValid(year, month, day);
        assertFalse(result);
    }
}
