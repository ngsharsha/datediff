package com.datediff.datediffcalculator;

public class DateUtils {
    
    public static long calculateDiff(Date date1, Date date2) {
        return Math.abs(date2.getJulianDay() - date1.getJulianDay());
    }
}
